/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package contador;

/**
 * @Nombre otros.java
 * @Autor GarcÃ­a HernÃ¡ndez IsaÃ­ Daniel
 * @Fecha 26/05/2011 11:43:03 PM
 */
public class Otros {

    /**
     * Histograma de la imagen
     */
    private int[] histograma = new int[256];
    /**
     * probabilidad de ocurrencia de un determinado pixel
     */
    private double[] probabilidadOcurrencia = new double[256];
    /**
     * Total de pixeles en la imagen
     */
    private double totalValores = 0.0;
    /**
     * Imagen que se va a tratar
     */
    private java.awt.image.BufferedImage imagen = null;
    /**
     * MEdia o esperanza matemÃ¡tica
     */
    private double media = 0;
    /**
     * Probabilidad acumulada de la region 0
     */
    private double proAcumulada0 = 0;
    /**
     * Probabilidad de la region 1
     */
    private double proAcumulada1 = 0;
    /**
     * Media de la region 0
     */
    private double mediaRegion0 = 0;
    /**
     * Media de la region 1
     */
    private double mediaRegion1 = 0;
    /**
     * Varianza de la region 0
     */
    private double varianzaRegion0 = 0;
    /**
     * Varianza de la region 1
     */
    private double varianzaRegion1 = 0;
    /**
     * Valor del umbral ideal
     */
    private int umbralIdeal = 0;

    /**
     * Constructor de la clase
     */
    public Otros() {
    }

    /**
     * Asignamos la imagen de la que se va a obtener el histograma
     * @param imagen Imagen actual
     */
    public void setImagen(java.awt.image.BufferedImage imagen) {
        //Asignamos el valor
        this.imagen = imagen;
        //recorremos el histograma
        for (int indice = 0; indice < histograma.length; indice++) {
            histograma[indice] = 0;
            probabilidadOcurrencia[indice] = 0.0;
        }
        //Recorremos la imagen
        for (int indiceX = 0; indiceX < imagen.getWidth(); indiceX++) {
            for (int indiceY = 0; indiceY < imagen.getHeight(); indiceY++) {
                //Tomamos el valor del pixel
                int pixel = imagen.getRGB(indiceX, indiceY);
                //Tomamos cada nivel de color RGB
                int rojo = ((pixel & 0x00FF0000) >> 16);
                int verde = ((pixel & 0x0000FF00) >> 8);
                int azul = (pixel & 0x000000FF);
                //Calculamos el promedio
                int total = ((rojo + verde + azul) / 3);
                //Calculamos el histograma
                histograma[total]++;
                //INcrementamos el total de valores
                totalValores++;
            }
        }

        //Calculamos los valores necesarios
        calcularValores();
    }

    /**
     * Calculamos los valores posibles de la imagen
     */
    private void calcularValores() {
        //Inicializamos las variables
        media = 0;

        //Recorremos el histograma
        for (int indice = 0; indice < histograma.length; indice++) {
            //Calculamos la probabildiad de cada valor de gris
            probabilidadOcurrencia[indice] = (double) histograma[indice] / totalValores;
        }
        //Recorremos las probabilidades
        for (int indice = 0; indice < probabilidadOcurrencia.length; indice++) {
            //Calculamos la media
            media += indice * probabilidadOcurrencia[indice];
            //System.out.println("Entropia: " + entropia + " Probabilidad: " + probabilidadOcurrencia[indice]);
        }
        //Calculamos los valores
        calcularUmbralIdeal(0, 0);
    }

    /**
     * Funcion recursiva que permite calcular el umbral optimo
     * @param umbral Umbral actualizado
     * @param optimo Valor optimo
     */
    private void calcularUmbralIdeal(int umbral, double optimo) {
        //Se inicializan los valores
        proAcumulada0 = 0;
        proAcumulada1 = 0;
        mediaRegion0 = 0;
        mediaRegion1 = 0;

        //Recorremos las probabilidades
        for (int indice = 0; indice < probabilidadOcurrencia.length; indice++) {
            //Si el indice es menor que el umbral
            if (indice <= umbral) {
                //Hacemos la sumatoria
                proAcumulada0 += probabilidadOcurrencia[indice];
                //mediaRegion0 += indice * probabilidadOcurrencia[indice];
            } else {
                //Hacemos la sumatoria
                proAcumulada1 += probabilidadOcurrencia[indice];
                //mediaRegion1 += indice * probabilidadOcurrencia[indice];
            }
        }


        //Recorremos las probabilidades
        for (int indice = 0; indice < probabilidadOcurrencia.length; indice++) {
            //Si el indice es menor que el umbral
            if (indice <= umbral) {
                if (proAcumulada0 == 0) {
                    mediaRegion0 = 0;
                } else {
                    mediaRegion0 += (indice * probabilidadOcurrencia[indice]) / proAcumulada0;
                }
            } else {
                if (proAcumulada1 == 0) {
                    mediaRegion1 = 0;
                } else {
                    mediaRegion1 += (indice * probabilidadOcurrencia[indice]) / proAcumulada1;
                }
            }
        }


        //System.out.println(((proAcumulada0 * mediaRegion0) + (proAcumulada1 * mediaRegion1)) + " == " + media);

        //Recorremos las probabilidades para calcular la varianza de cada region
        for (int indice = 0; indice < probabilidadOcurrencia.length; indice++) {
            //Si el indice es menor que el umbral
            if (indice <= umbral) {
                //Hacemos la sumatoria
                varianzaRegion0 += Math.pow((indice - mediaRegion0), 2) * probabilidadOcurrencia[indice];
            } else {
                //Hacemos la sumatoria
                varianzaRegion1 += Math.pow((indice - mediaRegion1), 2) * probabilidadOcurrencia[indice];
            }
        }

        //Se calculan las varianzas
        double varianzaZW = (proAcumulada0 * Math.pow((mediaRegion0 - media), 2)) + (proAcumulada1 * Math.pow((mediaRegion1 - media), 2));
        double varianzaIN = (proAcumulada0 * varianzaRegion0) + (proAcumulada1 * varianzaRegion1);
        //Se calcula la funcion a optimizar
        double funcionOptima = varianzaZW / varianzaIN;

        //System.out.println(umbralIdeal + " -> " + umbral + " -> " + optimo + " - " + funcionOptima + "\n");

        //Si el umbral es igual a 0
        if (umbral == 0) {
            //Se asignan los primeros valores
            setUmbralIdeal(umbral);
            optimo = funcionOptima;
        }

        //Si el optimo es menor que la funcion optima
        if (optimo < funcionOptima) {
            //Se actualizan los valores
            setUmbralIdeal(umbral);
            optimo = funcionOptima;
        }
        //Si el umbral es menor que 255
        if (umbral < 255) {
            umbral++;
            //Se manda llamar a la funcion recursiva
            this.calcularUmbralIdeal(umbral, optimo);
        }
    }

    /**
     * Permite hacer el cambio a escala de grices
     */
   
    /**
     * @param umbralIdeal the umbralIdeal to set
     */
    public void setUmbralIdeal(int umbralIdeal) {
        this.umbralIdeal = umbralIdeal;
    }

    /**
     * @return the umbralIdeal
     */
    public int getUmbralIdeal() {
        return umbralIdeal;
    }
}
