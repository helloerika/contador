/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package contador;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Yossi
 */
public class Analisis {

    // imagen
    private int[][] arregloImagen;
    private PuntoInicial[][] imagenAux;
    BufferedImage imagen;
    // declaramos variables imprtantes
    private int numeroRegiones = 0;
    private int[][] mascara;
    private int cX = 0;
    private int cY = 0;
    private int referenciaRegion = 0;
    private double euler;
    private double hoyos;

  
    public Analisis(BufferedImage imagen, int[][] conecti, int cX, int cY) {
        
        this.imagen = imagen;
       
        arregloImagen = imagenToArregloBinario(imagen, 0);
        arregloToDiscreto();
       
        this.mascara = conecti;
        this.cX = cX;
        this.cY = cY;
    }

    private List<PuntoInicial> findHermanos(int coordenadaX, int coordenadaY) {

      
        int[][] vecinos = {{0, 1, 0}, {1, 0, 1}, {0, 1, 0}};

       
        List<PuntoInicial> pixelesVecinos = new ArrayList<PuntoInicial>();

        int indiceX = 0;
        int indiceY = 0;

      
        for (int indiceI = (coordenadaX - 1); indiceI < (coordenadaX + 2); indiceI++) {

           
            if (indiceI >= 0 && indiceI < imagenAux.length) {
                for (int indiceJ = (coordenadaY - 1); indiceJ < (coordenadaY + 2); indiceJ++) {
                
                    if (indiceJ >= 0 && indiceJ < imagenAux[indiceI].length) {

                      
                        if (imagenAux[indiceI][indiceJ].getValor() == 1 && vecinos[indiceX][indiceY] == 1) {
                            if (imagenAux[indiceI][indiceJ].getNumeroRegion() > imagenAux[coordenadaX][coordenadaY].getNumeroRegion()) {
                            
                                PuntoInicial nuevoVecino = new PuntoInicial();
                           
                                nuevoVecino.setCoordenadaX(indiceI);
                             
                                nuevoVecino.setCoordenadaY(indiceJ);
                              
                                nuevoVecino.setValor(1);
                            
                                pixelesVecinos.add(nuevoVecino);
                            }
                        }
                    }

                    indiceY++;
                }
            }

            indiceX++;
            indiceY = 0;
        }
       
        return pixelesVecinos;
    }

    private int detRegionesFinales() {
     
        List<Integer> listaEtiquetas = new ArrayList<Integer>();
      
        int etiquetaActual = 0;

     
        for (int indiceI = 0; indiceI < imagenAux.length; indiceI++) {
            for (int indiceJ = 0; indiceJ < imagenAux[indiceI].length; indiceJ++) {
            
                if (imagenAux[indiceI][indiceJ].getNumeroRegion() > 0) {
                  
                    etiquetaActual = imagenAux[indiceI][indiceJ].getNumeroRegion();
                  
                    if (!verificarExistencia(listaEtiquetas, etiquetaActual)) {

                       
                        listaEtiquetas.add(etiquetaActual);
                    }
                }
            }
        }


        for (int indiceI = 0; indiceI < imagenAux.length; indiceI++) {
            for (int indiceJ = 0; indiceJ < imagenAux[indiceI].length; indiceJ++) {
              
                if (imagenAux[indiceI][indiceJ].getNumeroRegion() > 0) {
               
                    imagenAux[indiceI][indiceJ].setNumeroRegion(listaEtiquetas.indexOf(imagenAux[indiceI][indiceJ].getNumeroRegion()) + 1);
                }

                
            }
        
        }

        return listaEtiquetas.size();
    }

 
    private boolean verificarExistencia(List<Integer> lista, int etiqueta) {
        boolean existe = false;
        for (Integer etiquetaActual : lista) {
            if (etiquetaActual == etiqueta) {

                existe = true;
                break;
            }
        }
        return existe;
    }

    
    private int erosionPixel(int indiceX, int indiceY) {

        int valorFinal = 1;
        ElementoEstructural:
        for (int indiceI = 0; indiceI < mascara.length; indiceI++) {

            if (indiceI >= 0 && indiceI < arregloImagen.length) {
                for (int indiceJ = 0; indiceJ < mascara[indiceI].length; indiceJ++) {

                    if (indiceJ >= 0 && indiceJ < arregloImagen[indiceI].length) {

                        if (mascara[indiceI][indiceJ] == 1 && (mascara[indiceI][indiceJ] != arregloImagen[indiceX - cX + indiceI][indiceY - cY + indiceJ])) {
                            valorFinal = 0;

                            break ElementoEstructural;
                        }
                    }
                }
            }
        }


        return valorFinal;
    }

    public BufferedImage erosion(BufferedImage imagen) {


        int[][] arregloBinario = imagenToArregloBinario(imagen, 6);
        int[][] arregloErosionado = imagenToArregloBinario(imagen, 6);


        for (int indiceX = 0; indiceX < imagen.getWidth(); indiceX++) {
            for (int indiceY = 0; indiceY < imagen.getHeight(); indiceY++) {

                if (arregloBinario[indiceX][indiceY] == 1) {

                    ElementoEstructural:
                    for (int indiceI = 0; indiceI < mascara.length; indiceI++) {
                        for (int indiceJ = 0; indiceJ < mascara[indiceI].length; indiceJ++) {

                            if (mascara[indiceI][indiceJ] == 1 && (mascara[indiceI][indiceJ] != arregloBinario[indiceX - cX + indiceI][indiceY - cY + indiceJ])) {

                                arregloErosionado[indiceX][indiceY] = 0;

                                break ElementoEstructural;
                            }
                        }
                    }
                }
            }
        }


        //Creamos una copia del bufer.
        java.awt.image.BufferedImage nuevaImagen = this.arregloToImagen(arregloErosionado);
        //Retornamos la imagen en escala de grises
        return nuevaImagen;
    }

    private int calcularCaras(int coordenadaX, int coordenadaY) {


        int[][] esquinas = {{1, 0, 1}, {0, 1, 0}, {1, 0, 1}};
        int totalLados = 0;


        int indiceX = 0;
        int indiceY = 0;


        for (int indiceI = (coordenadaX - 1); indiceI < (coordenadaX + 2); indiceI++) {


            if (indiceI >= 0 && indiceI < arregloImagen.length) {
                for (int indiceJ = (coordenadaY - 1); indiceJ < (coordenadaY + 2); indiceJ++) {

                    if (indiceJ >= 0 && indiceJ < arregloImagen[indiceI].length) {


                        if (arregloImagen[indiceI][indiceJ] == 0 && esquinas[indiceX][indiceY] == 0) {
                            totalLados++;
                        }
                    }

                    indiceY++;
                }
            }

            indiceX++;
            indiceY = 0;
        }

        return totalLados;
    }

    public void procesarImagen() {


        int numPR = 0;
        int numMPixels = 0;
        int numCaras = 0;
        referenciaRegion = 0;

        for (int indiceI = 0; indiceI < arregloImagen.length; indiceI++) {
            for (int indiceJ = 0; indiceJ < arregloImagen[indiceI].length; indiceJ++) {

                if (arregloImagen[indiceI][indiceJ] == 1) {

                    numPR++;

                    numMPixels += erosionPixel(indiceI, indiceJ);

                    numCaras += calcularCaras(indiceI, indiceJ);

                    if (!imagenAux[indiceI][indiceJ].isVisitado()) {
                        labelObtener(indiceI, indiceJ);
                    }
                }
            }
        }

        numeroRegiones = detRegionesFinales();

        euler = (double) numMPixels - (((2.0 * (double) numPR) - (double) numCaras) / 2.0);
        hoyos = (((2.0 * (double) numPR) - (double) numCaras) / 2.0) - (double) numMPixels + (double) numeroRegiones;


    }

    private void labelObtener(int coordenadaX, int coordenadaY) {

        int[][] vecinos = {{0, 1, 0}, {1, 0, 1}, {0, 1, 0}};

        int indiceX = 0;
        int indiceY = 0;

        int valorMenor = referenciaRegion;
        boolean inicio = true;


        for (int indiceI = (coordenadaX - 1); indiceI < (coordenadaX + 2); indiceI++) {


            if (indiceI >= 0 && indiceI < imagenAux.length) {
                for (int indiceJ = (coordenadaY - 1); indiceJ < (coordenadaY + 2); indiceJ++) {

                    if (indiceJ >= 0 && indiceJ < imagenAux[indiceI].length) {


                        if (imagenAux[indiceI][indiceJ].getValor() == 1 && vecinos[indiceX][indiceY] == 1) {


                            if (!(indiceI == coordenadaX && indiceJ == coordenadaY)) {
                                if (imagenAux[indiceI][indiceJ].getNumeroRegion() > 0) {
                                    if (imagenAux[indiceI][indiceJ].getNumeroRegion() < valorMenor) {
                                        valorMenor = imagenAux[indiceI][indiceJ].getNumeroRegion();
                                    }

                                    inicio = false;
                                }
                            }
                        }
                    }

                    indiceY++;
                }
            }

            indiceX++;
            indiceY = 0;
        }

        if (inicio) {
            imagenAux[coordenadaX][coordenadaY].setVisitado(true);
            imagenAux[coordenadaX][coordenadaY].setNumeroRegion(++referenciaRegion);
        } else {
            imagenAux[coordenadaX][coordenadaY].setVisitado(true);
            imagenAux[coordenadaX][coordenadaY].setNumeroRegion(valorMenor);
            changeNeigbors(coordenadaX, coordenadaY);
        }

    }

    private void changeNeigbors(int coordenadaX, int coordenadaY) {

        List<PuntoInicial> vecinosMayores = findHermanos(coordenadaX, coordenadaY);
        //Recorremos el arreglo de pixeles
        for (PuntoInicial pixelActual : vecinosMayores) {

            imagenAux[pixelActual.getCoordenadaX()][pixelActual.getCoordenadaY()].setVisitado(true);
            imagenAux[pixelActual.getCoordenadaX()][pixelActual.getCoordenadaY()].setNumeroRegion(imagenAux[coordenadaX][coordenadaY].getNumeroRegion());

            changeNeigbors(pixelActual.getCoordenadaX(), pixelActual.getCoordenadaY());
        }
    }

    private void arregloToDiscreto() {

        imagenAux = new PuntoInicial[arregloImagen.length][arregloImagen[0].length];

        for (int indiceI = 0; indiceI < arregloImagen.length; indiceI++) {
            for (int indiceJ = 0; indiceJ < arregloImagen[indiceI].length; indiceJ++) {


                PuntoInicial nuevoPixel = new PuntoInicial();

                nuevoPixel.setCoordenadaX(indiceI);
                nuevoPixel.setCoordenadaY(indiceJ);

                nuevoPixel.setValor(arregloImagen[indiceI][indiceJ]);

                imagenAux[indiceI][indiceJ] = nuevoPixel;
            }
        }
    }

    public int getNumeroRegiones() {
        return numeroRegiones;
    }

    public double getEuler() {
        return euler;
    }

    public double getHoyos() {
        return hoyos;
    }

    private int[][] imagenToArregloBinario(BufferedImage imagen, int incremento) {


        int[][] arregloImagen = new int[imagen.getWidth() + (incremento / 2)][imagen.getHeight() + (incremento / 2)];


        for (int indiceX = (incremento / 2); indiceX < imagen.getWidth(); indiceX++) {
            for (int indiceY = (incremento / 2); indiceY < imagen.getHeight(); indiceY++) {

                int pixel = imagen.getRGB(indiceX - (incremento / 2), indiceY - (incremento / 2));

                int rojo = ((pixel & 0x00FF0000) >> 16);
                int verde = ((pixel & 0x0000FF00) >> 8);
                int azul = (pixel & 0x000000FF);

                int total = ((rojo + verde + azul) / 3);

                if (total == 0) {

                    arregloImagen[indiceX][indiceY] = 1;
                } else {

                    arregloImagen[indiceX][indiceY] = 0;
                }

            }
        }


        return arregloImagen;
    }

    private BufferedImage arregloToImagen(int[][] arreglo) {

        int ancho = arreglo.length;
        int alto = arreglo[0].length;


        java.awt.image.BufferedImage nuevaImagen = new java.awt.image.BufferedImage(ancho, alto, java.awt.image.BufferedImage.TYPE_INT_ARGB);


        for (int indiceX = 0; indiceX < ancho; indiceX++) {
            for (int indiceY = 0; indiceY < alto; indiceY++) {

                int pixel = 255;

                if (arreglo[indiceX][indiceY] == 1) {

                    pixel = 0;
                }

                int colorRGB = (0xff000000 | (pixel << 16) | (pixel << 8) | pixel);

                nuevaImagen.setRGB(indiceX, indiceY, colorRGB);
            }
        }

        //Retornamos la imagen
        return nuevaImagen;
    }
    
    public BufferedImage erosionar(BufferedImage imagenOriginal) {

        BufferedImage imagen = new BufferedImage(imagenOriginal.getWidth(null), imagenOriginal.getHeight(null), BufferedImage.TYPE_INT_RGB);
        imagen.createGraphics().drawImage(imagenOriginal, 0, 0, null);

        BufferedImage erosion = new BufferedImage(imagenOriginal.getWidth(null), imagenOriginal.getHeight(null), BufferedImage.TYPE_INT_RGB);
        erosion.createGraphics().drawImage(imagenOriginal, 0, 0, null);


        for (int x = 0; x < imagen.getHeight(); x++) {
            for (int y = 0; y < imagen.getWidth(); y++) {
                if (((imagen.getRGB(y, x) & 0x00FF0000) >> 16) == 0) {
                    int posicion1 = ((imagen.getRGB(y, x + 1) & 0x00FF0000) >> 16);
                    int posicion2 = ((imagen.getRGB(y + 1, x + 1) & 0x00FF0000) >> 16);
                    int posicion3 = ((imagen.getRGB(y + 1, x) & 0x00FF0000) >> 16);
                    if (posicion1 == 255 || posicion2 == 255 || posicion3 == 255) {
                        imagen.setRGB(y, x, (0xff000000 | (255 << 16) | (255 << 8) | 255));
                    }
                }
            }
        }

        BufferedImage modificada = new BufferedImage(imagen.getWidth(), imagen.getHeight(), Image.SCALE_DEFAULT);

        return modificada;
    }
}
