/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package contador;

import java.awt.Image;
import java.awt.image.BufferedImage;


/**
 *
 * @author canaleija
 */
public class Filtros {

    public Filtros() {
    }

    public Image erosionar(Image imagenOriginal) {

        BufferedImage imagen = new BufferedImage(imagenOriginal.getWidth(null), imagenOriginal.getHeight(null), BufferedImage.TYPE_INT_RGB);
        imagen.createGraphics().drawImage(imagenOriginal, 0, 0, null);

        BufferedImage erosion = new BufferedImage(imagenOriginal.getWidth(null), imagenOriginal.getHeight(null), BufferedImage.TYPE_INT_RGB);
        erosion.createGraphics().drawImage(imagenOriginal, 0, 0, null);


        for (int x = 0; x < imagen.getHeight(); x++) {
            for (int y = 0; y < imagen.getWidth(); y++) {
                if (((imagen.getRGB(y, x) & 0x00FF0000) >> 16) == 0) {
                    int posicion1 = ((imagen.getRGB(y, x + 1) & 0x00FF0000) >> 16);
                    int posicion2 = ((imagen.getRGB(y + 1, x + 1) & 0x00FF0000) >> 16);
                    int posicion3 = ((imagen.getRGB(y + 1, x) & 0x00FF0000) >> 16);
                    if (posicion1 == 255 || posicion2 == 255 || posicion3 == 255) {
                        imagen.setRGB(y, x, (0xff000000 | (255 << 16) | (255 << 8) | 255));
                    }
                }
            }
        }

        Image modificada = imagen.getScaledInstance(imagen.getWidth(), imagen.getHeight(), Image.SCALE_DEFAULT);

        return modificada;
    }
}
