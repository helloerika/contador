/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package contador;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.font.FontRenderContext;
import java.awt.font.TextLayout;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;

/**
 * @Nombre Pixel.java
 * @Autor García Hernández Isaí Daniel
 * @Fecha 6/09/2011 06:47:52 PM
 */
public class PuntoInicial {

    /**
     * Valor de la coordenada X
     */
    private int coordenadaX = 0;
    /**
     * Valor de la coordenada Y
     */
    private int coordenadaY = 0;
    /**
     * defiine si el pixel ya ha sido visitado o no
     */
    private boolean visitado = false;
    /**
     * Define el valor del pixel
     */
    private int valor = 0;
    /**
     * Tipo de pixel
     */
    private PuntosIniciales tipoPixel = PuntosIniciales.FONDO;
    /**
     * Grafico del nodo
     */
    private Shape figura;

    private int numeroRegion = 0;

    /**
     * Constructor de la clase
     */
    public PuntoInicial() {

    }

    /**
     * Dibujamos la figura
     * @param g2 Fuente de graficos
     */
    public void draw(Graphics2D g2) {
        //Se obtienen las coordenadas de la figura
        double coordenedaX = figura.getBounds2D().getCenterX();
        double coordenedaY = figura.getBounds2D().getCenterY();

        //Se establece el color de los graficos
        g2.setColor(Color.BLACK);
        //Se dibuja el contorno de la figura
        g2.draw(figura);
        //Se define el color del fondo de acuerdo al tipo de pixel,
        switch(tipoPixel) {
            case INTERNO:
                g2.setColor(Color.BLACK);
                break;
            case FONDO:
            case FONDOINTERNO:
                g2.setColor(Color.WHITE);
                break;
            case CONTORNOEXTERNO:
                g2.setColor(Color.GRAY);
                break;
            case CONTORNOINTERNO:
                g2.setColor(Color.LIGHT_GRAY);
                break;
            case VECINO:
                g2.setColor(Color.GREEN);
                break;
        }
        //Se dobuja el fondo
        g2.fill(figura);
        //Se establece el color de los graficos
        g2.setColor(Color.WHITE);
        //Se crea la instancia de la clase FontRenderCpontex
        FontRenderContext frcRender = g2.getFontRenderContext();
        //Se establece el tipo de fuente
        Font fuente = new Font("Timer", Font.BOLD, 11);
        //Se crea la cadena con el valor del pixel
        String texto = "" + this.getValor();
        ////Se crea la varible para poder dibujar el tecto
        TextLayout txtlTexto = new TextLayout(texto, fuente, frcRender);
        //Se dibuja el texto
        txtlTexto.draw(g2, (float)coordenedaX - 3, (float)coordenedaY + 3);
    }

    /**
     * Establece si contiene un punto
     * @param p2 Punto que se esta buscando
     * @return Si tiene el punto o no
     */
    public boolean contiene(Point2D p2) {
        return figura.contains(p2);
    }

    /**
     * Establece si contiene un punto
     * @param p Punto que se esta buscando
     * @param at Transformacion
     * @return Si tiene el punto o no
     */
    public boolean contiene(Point2D p, AffineTransform at) {
        //Establecemos la transformacion
        Shape s = at.createTransformedShape(figura);
        return s.contains(p);
    }

    /**
     * @return the coordenadaX
     */
    public int getCoordenadaX() {
        return coordenadaX;
    }

    /**
     * @param coordenadaX the coordenadaX to set
     */
    public void setCoordenadaX(int coordenadaX) {
        this.coordenadaX = coordenadaX;
    }

    /**
     * @return the coordenadaY
     */
    public int getCoordenadaY() {
        return coordenadaY;
    }

    /**
     * @param coordenadaY the coordenadaY to set
     */
    public void setCoordenadaY(int coordenadaY) {
        this.coordenadaY = coordenadaY;
    }

    /**
     * @return the visitado
     */
    public boolean isVisitado() {
        return visitado;
    }

    /**
     * @param visitado the visitado to set
     */
    public void setVisitado(boolean visitado) {
        this.visitado = visitado;
    }

    /**
     * @return the valor
     */
    public int getValor() {
        return valor;
    }

    /**
     * @param valor the valor to set
     */
    public void setValor(int valor) {
        this.valor = valor;
        //Si el valor es mayor que 0
        if(this.valor > 0) {
            //El pixel es tipo interno
            this.setTipoPixel(PuntosIniciales.INTERNO);
        }
    }

    /**
     * @return the tipoPixel
     */
    public PuntosIniciales getTipoPixel() {
        return tipoPixel;
    }

    /**
     * @param tipoPixel the tipoPixel to set
     */
    public void setTipoPixel(PuntosIniciales tipoPixel) {
        this.tipoPixel = tipoPixel;
    }

    /**
     * @return the figura
     */
    public Shape getFigura() {
        return figura;
    }

    /**
     * @param figura the figura to set
     */
    public void setFigura(Shape figura) {
        this.figura = figura;
    }

    /**
     * @return the numeroRegion
     */
    public int getNumeroRegion() {
        return numeroRegion;
    }

    /**
     * @param numeroRegion the numeroRegion to set
     */
    public void setNumeroRegion(int numeroRegion) {
        this.numeroRegion = numeroRegion;
    }

}
