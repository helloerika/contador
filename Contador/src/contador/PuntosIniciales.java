/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package contador;

/**
 * @Nombre TipoPixel.java
 * @Autor García Hernández Isaí Daniel
 * @Fecha 15/09/2011 11:27:45 AM
 */
public enum PuntosIniciales {

    /**
     * Se definen lso tipos enumerados
     */
    CONTORNOEXTERNO("Pixel de contorno externo"),
    CONTORNOINTERNO("Pixel de contorno Interno"),
    INTERNO("Pixel interno"),
    FONDO("Fondo de la imagen"),
    FONDOINTERNO("Fondo interno de la region"),
    VECINO("Vecino");


    /**
     * Variable que permite tomar el valor del enumerado
     */
    private String tipoPixel;

    /**
     * Cosntructor del tipo enumerado
     * @param tipoPixel Tipo de pixel requerido
     */
    private PuntosIniciales(String tipoPixel) {
        //Se asigna el vlaor
        this.tipoPixel = tipoPixel;
    }

    /**
     * PErmite obtener el tipo de pixel
     * @return
     */
    public String getTipoPixel() {
        //retornamos el tipo de pixel
        return tipoPixel;
    }
}
