/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaz;

import java.awt.image.BufferedImage;
import java.io.IOException;

/**
 *
 * @author canaleija
 */
public class abrirImagen {

    public BufferedImage abrirImagen() throws IOException {

       

        javax.swing.filechooser.FileNameExtensionFilter filter = new javax.swing.filechooser.FileNameExtensionFilter("Imagenes", "png", "jpg", "bmp");

        javax.swing.JFileChooser seleccion = new javax.swing.JFileChooser();

        seleccion.addChoosableFileFilter(filter);

        seleccion.setFileSelectionMode(javax.swing.JFileChooser.FILES_ONLY);

        int resultado = seleccion.showOpenDialog(null);

        if (resultado == javax.swing.JFileChooser.CANCEL_OPTION) {
            return null;
        }
        //Cargamos el archivo
        java.io.File archivo = seleccion.getSelectedFile();

        //Convertimos la imagen en un bufferImage
        BufferedImage imagen = javax.imageio.ImageIO.read(archivo);


        return imagen;
    }
}
